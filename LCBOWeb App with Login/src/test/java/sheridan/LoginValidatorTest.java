package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginCharactersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rishabhbajaj2001" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "r@!#1sh@aa" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "2312345" ) );
	}
	
	@Test
	public void testIsValidLoginCharactersBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "rishabh@2001" ) );
	}
	
	@Test
	public void testIsValidLoginNumCharRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "rishabhbajaj" ) );
	}
	
	@Test
	public void testIsValidLoginNumCharException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( null ) );
	}
	
	@Test
	public void testIsValidLoginNumCharBoundryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginNumCharBoundryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "karen" ) );
	}

}
