package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName!=null && loginName.length( ) > 5 && loginName.matches("[a-zA-Z0-9]*");
	}
}
